# DummyLauncher

## Goal

Create a catapult to throw a dummy for a hunting dog.

## Implementation

**Important note: I'm not an Android professional.**

- Wooden catapult, armed and locked with a strong rubber band
- Wemos D1 Mini (ESP8266) with separate BLE module (HM-10) and servo (a spare FS5103B)
- Android app connects via BLE
- Sends a 'launch' signal over UART
- ESP receives command over UART via SoftSerial and sweeps the servo, unlocking the rubber band
- Dummy flies
- Dog happy

## Prior art

- A fantastic BT tutorial - https://punchthrough.com/android-ble-guide/
- HM-10 Service and Characteristic UUIDs - http://blog.blecentral.com/2015/05/05/hm-10-peripheral/
