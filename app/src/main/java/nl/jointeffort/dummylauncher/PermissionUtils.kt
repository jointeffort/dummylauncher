package nl.jointeffort.dummylauncher

import android.Manifest
import android.app.Activity
import android.content.pm.PackageManager
import android.util.Log
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat

class PermissionUtils {

    companion object {
        private const val REQUEST_PERMISSIONS = 1

        private val requiredPermissions = arrayOf(
            Manifest.permission.BLUETOOTH,
            Manifest.permission.BLUETOOTH_ADMIN,
            Manifest.permission.ACCESS_FINE_LOCATION,
            Manifest.permission.ACCESS_COARSE_LOCATION,
            Manifest.permission.BLUETOOTH_CONNECT,
            Manifest.permission.BLUETOOTH_SCAN,
            Manifest.permission.BLUETOOTH_ADMIN
        )

        fun checkAndRequestPermissions(activity: Activity) {
            val notGrantedPermissions = mutableListOf<String>()

            // Check each required permission
            for (permission in requiredPermissions) {
                if (ContextCompat.checkSelfPermission(
                        activity,
                        permission
                    ) != PackageManager.PERMISSION_GRANTED
                ) {
                    Log.d(TAG, "Permission ${permission} is missing")
                    notGrantedPermissions.add(permission)
                }
            }

            // If there are permissions to request, ask the user
            if (notGrantedPermissions.isNotEmpty()) {
                ActivityCompat.requestPermissions(
                    activity,
                    notGrantedPermissions.toTypedArray(),
                    REQUEST_PERMISSIONS
                )
            } else {
                Log.d("DummyLauncher", "All required permissions granted")
            }
        }
    }
}